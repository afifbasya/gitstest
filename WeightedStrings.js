function weightedStrings(s, queries) {
    let dict = [];
    let alphabets = 'abcdefghijklmnopqrstuvwxyz';
    let alphabet2 = alphabets.split('');
    for (let j = 0; j < alphabet2.length; j++) {
        dict.push({
            key: alphabet2[j],
            value: j + 1
        })
    }
    let previous = '';
    let newQuery = [];
    for (let i = 0; i < s.length; i++) {
        let weight = 0;
        if (previous.includes(s[i])) {
            previous = previous + s[i];
            for (let k = 0; k < dict.length; k++) {
                if (dict[k].key == s[i]) {
                    weight = dict[k].value * previous.length;
                    newQuery.push(weight);
                }
            }
        } else {
            previous = s[i];
            for (let k = 0; k < dict.length; k++) {
                if (dict[k].key == s[i]) {
                    weight = dict[k].value;
                    newQuery.push(weight);
                }
            }
        }
    }
    let result = [];
    for (let z = 0; z < queries.length; z++) {
        if (newQuery.includes(queries[z])) {
            result.push("Yes")
        }
        else {
            result.push("No");
        }
    }
    return result;
}

console.log(weightedStrings("abbcccd", [1, 3, 9, 8]))